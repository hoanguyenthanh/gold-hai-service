/**
 * 
 */
package server.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author HOA
 *
 */
public class ServletContextListenerImp implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent e) {
	}
	
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			
			//ServletContext context = event.getServletContext();
			
//			String source = context.getInitParameter("data-source");
//			String user = context.getInitParameter("username");
//			String pass = context.getInitParameter("password");
//			
//			Connection connect = DriverManager.getConnection("jdbc:jtds:sqlserver://" + source, user, pass);
//			context.setAttribute("dbconnect", connect);
			
		} catch (Exception e) {
			System.out.println("ServletContextListenerImp connect ERROR: " + e.getLocalizedMessage());
		}
	}

}
