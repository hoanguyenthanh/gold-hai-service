package server.https;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;

public class GetGroup extends BaseHttpServlet {
	private static final long serialVersionUID = -6468233831770179555L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONArray jsonArray = (JSONArray) getGroupList();
		PrintWriter out = response.getWriter();
		
		out.print(jsonArray);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		doGet(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected JSONArray getGroupList() {
		
		JSONArray jsonGroup = new JSONArray();
		try{
			mStatement = this.getConnectionDb().createStatement();
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("SELECT ");
			strSql.append("	 GroupID ");
			strSql.append("	,GroupCode ");
		    strSql.append("	,GroupName ");
			strSql.append("FROM WS_GROUP ");
			strSql.append("WHERE Active = '1' ");
			
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			if (mResultSet != null){
				while(mResultSet.next()){
					JSONObject jsonObject = new JSONObject();
				
					jsonObject.put("GroupID", mResultSet.getString("GroupID"));
					jsonObject.put("GroupCode", mResultSet.getString("GroupCode"));
					jsonObject.put("GroupName", mResultSet.getString("GroupName"));
			
					jsonGroup.add(jsonObject);
				}
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			this.closeStatement();
		}
		
		return jsonGroup;
	}
}