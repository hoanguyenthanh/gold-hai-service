package server.https.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public abstract class BaseHttpServlet extends HttpServlet{
	private static final long serialVersionUID = -4983551623061525386L;

	protected Connection mConnectionDb = null;
	
	protected Statement mStatement = null;

	protected ResultSet mResultSet = null;
	
	protected PreparedStatement mPreparedStatement = null;
	
	protected JsonResponse mJsonResponse;
	
	
	protected ServletContext mContext;
	protected static String mHost, mDataSource, mUserName, mPassword;
	
	@Override
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		try {
			req.setCharacterEncoding("UTF-8");
			res.setContentType("application/json");
			res.setCharacterEncoding("UTF-8");
			
			mContext = getServletContext();
			mHost = mContext.getInitParameter("host");
			mDataSource = mContext.getInitParameter("data-source");
			mUserName = mContext.getInitParameter("username");
			mPassword = mContext.getInitParameter("password");
			mJsonResponse = new JsonResponse();
			
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		super.service(req, res);
	}
	
	public Connection getConnectionDb (){
		try {
			mConnectionDb = DriverManager.getConnection("jdbc:jtds:sqlserver://" + mHost + "/" + mDataSource, mUserName, mPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mConnectionDb;
	}
	
	public Connection getConnection(){
		Connection connect = null;
		try {
			connect = DriverManager.getConnection("jdbc:jtds:sqlserver://" + mHost + "/" + mDataSource, mUserName, mPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connect;
	}
	
	public Connection getConnectDbOLD(){
		try {
			mConnectionDb = DriverManager.getConnection("jdbc:jtds:sqlserver://" + mHost + "/" + "BH_HAI", mUserName, mPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mConnectionDb;	
	}
	
	
	protected void closeStatement(){
		try {
			if (mStatement != null){
				mStatement.close();
				mStatement = null;
			}
		} catch (SQLException e) {
			System.out.println("BaseDao close statementERROR: " + e.getLocalizedMessage());
		}
		
		try{
			if (mPreparedStatement != null){
				mPreparedStatement.close();
				mPreparedStatement = null;
			}
		} catch (SQLException e){
			System.out.println("BaseDao close preparedStatement ERROR: " + e.getLocalizedMessage());
		}
		
		try
		{
			if (mResultSet != null){
				mResultSet.close();
				mResultSet = null;
			}
		} catch (SQLException e){
			System.out.println("BaseDao close resultSet ERROR: " + e.getLocalizedMessage());
		}
		
		try{
			if (mConnectionDb != null && !mConnectionDb.isClosed()){
				mConnectionDb.close();
				mConnectionDb = null;
			}
		} catch (SQLException e) {
			System.out.println("BaseDao close mConnection ERROR: " + e.getLocalizedMessage());
		}
	}
}