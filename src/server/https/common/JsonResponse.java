package server.https.common;

import org.json.simple.JSONObject;

@SuppressWarnings({ "unchecked", "serial" })
public class JsonResponse extends JSONObject{
	public String getAction() {
		return this.get("action").toString();
	}
	
	public int getMsgCode() {
		int value = Integer.parseInt(this.get("msgCode").toString());
		return value;
	}
	
	public String getMsgString() {
		return this.get("msgString").toString();
	}
	
	public Object getResult() {
		return this.get("result");
	}
	
	public void setAction(String action) {
		this.put("action", action);
	}
	
	public void setMsgCode(int msgCode) {
		this.put("msgCode", msgCode);
	}
	
	public void setMsgString(String msgString) {
		this.put("msgString", msgString);
	}
	
	public void setResult(Object result) {
		this.put("result", result);
	}
	
	public void setResultDetail(Object resultDT) {
		this.put("resultDetail", resultDT);
	}
	
	public void setStatus(String sts){
		this.put("status", sts);
	}
}
