package server.https;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;


@WebServlet(description = "Get gold type object by gold code", urlPatterns = {"/GetGoldTypeObject"})
public class GetGoldTypeObject extends BaseHttpServlet {



	/**
	 * 
	 */
	private static final long serialVersionUID = 3834421173180553653L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String goldCode = request.getParameter("GoldType").toString();
		
		JSONObject data = getGoldType(goldCode);
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	
	@SuppressWarnings("unchecked")
	protected JSONObject getGoldType(String goldCode) {
		
		JSONObject goldType = new JSONObject();
		
		try{
			mConnectionDb = this.getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("SELECT ");
			strSql.append("	 GoldCode ");
			strSql.append("	,GoldDesc ");
		    strSql.append("	,GoldType ");
		    strSql.append("	,HS ");
		    strSql.append("	,PriceSell ");
		    strSql.append("	,PriceBuy ");
			strSql.append("FROM WS_TYPE ");
			strSql.append("WHERE GoldCode = '" + goldCode + "'");
			
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			while(mResultSet.next()){
				HashMap<String, Object> hashMap = new HashMap<>();
				hashMap.put("GoldCode", mResultSet.getString("GoldCode"));
				hashMap.put("GoldDesc", mResultSet.getString("GoldDesc"));
				hashMap.put("GoldType", mResultSet.getString("GoldType"));
				hashMap.put("HS", mResultSet.getBigDecimal("HS"));
				hashMap.put("PriceSell", mResultSet.getBigDecimal("PriceSell"));
				hashMap.put("PriceBuy", mResultSet.getBigDecimal("PriceBuy"));
				
				goldType.putAll(hashMap);
				break;
			}
	
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			this.closeStatement();
		}
		
		return goldType;
	}
}
