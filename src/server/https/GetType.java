package server.https;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;

import server.https.common.BaseHttpServlet;


public class GetType extends BaseHttpServlet {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 17312576267001040L;




	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONArray data = getTypeList();
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	
	@SuppressWarnings("unchecked")
	protected JSONArray getTypeList() {
		
		JSONArray listType = new JSONArray();
		try{
			mConnectionDb = this.getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("SELECT ");
			strSql.append("	 GoldCode ");
			strSql.append("	,GoldDesc ");
		    strSql.append("	,GoldType ");
		    strSql.append("	,HS ");
		    strSql.append("	,PriceSell ");
		    strSql.append("	,PriceBuy ");
			strSql.append("FROM WS_TYPE ");
			strSql.append("WHERE Active = '1'");
			
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			if (mResultSet != null){
				while(mResultSet.next()){
					HashMap<String, Object> hashMap = new HashMap<>();
					hashMap.put("GoldCode", mResultSet.getString("GoldCode"));
					hashMap.put("GoldDesc", mResultSet.getString("GoldDesc"));
					hashMap.put("GoldType", mResultSet.getString("GoldType"));
					hashMap.put("HS", mResultSet.getBigDecimal("HS"));
					hashMap.put("PriceSell", mResultSet.getBigDecimal("PriceSell"));
					hashMap.put("PriceBuy", mResultSet.getBigDecimal("PriceBuy"));
					
					listType.add(hashMap);
				}
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			this.closeStatement();
		}
		
		return listType;
	}
}
