package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;

public class GetOrder extends BaseHttpServlet {
	private static final long serialVersionUID = 7058679741234498493L;
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		
		long orderId = Long.parseLong(req.getParameter("OrderId").toString());
		JSONObject data = this.getOrder(orderId);
		
		PrintWriter out = resp.getWriter();
		out.println(data);
		out.close();
	}
	
	
	@SuppressWarnings("unchecked")
	protected JSONObject getOrder(long orderId) {
		JSONObject dataResult = new JSONObject();
		
		try{
			StringBuilder strSql = new StringBuilder();
			
			  strSql.append("SELECT ");
			  strSql.append("	 OrderId ");
			  strSql.append("	,ModUser ");
			  strSql.append("	,ModDt ");
			  strSql.append("	,CONVERT(VARCHAR(10), ModDt, 105)  AS ModDate");
			  strSql.append("	,CONVERT(VARCHAR(5), ModDt, 8) AS ModTime");
			  strSql.append("	,TypeTrans ");
			  strSql.append("	,Status ");
			  strSql.append("	,Status AS StatusName ");
			  strSql.append("FROM WS_ORDER ");
			  strSql.append("WHERE OrderId = " + orderId);
			
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			if (mResultSet != null && mResultSet.next()){
					dataResult.put("OrderId", mResultSet.getLong("OrderId"));
					dataResult.put("ModUser", mResultSet.getString("ModUser"));
					dataResult.put("ModDt", mResultSet.getDate("ModDt").toString());
					dataResult.put("ModDate", mResultSet.getString("ModDate"));
					dataResult.put("ModTime", mResultSet.getString("ModTime"));
					dataResult.put("TypeTrans", mResultSet.getString("TypeTrans"));
					dataResult.put("Status", mResultSet.getString("Status"));
					dataResult.put("StatusName", mResultSet.getString("StatusName"));
			}
		} catch(SQLException e){
			dataResult = null;
		} finally {
			this.closeStatement();
		}
		
		return dataResult;
	}
}
