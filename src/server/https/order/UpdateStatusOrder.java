package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.https.common.BaseHttpServlet;


@WebServlet(description = "Xác nhận đã thanh toán", urlPatterns = { "/UpdateStatusOrder" })
public class UpdateStatusOrder extends BaseHttpServlet {
	private static final long serialVersionUID = 8698511586146315499L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		long orderId = Long.valueOf(request.getParameter("OrderId"));
		String status = request.getParameter("Status").toString();
		
		int mResultCode = -1;
		try{
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("	UPDATE WS_ORDER SET ");
			strSql.append("		Status = '" + status + "'");
			strSql.append("	    ,ModDt = GETDATE() ");
			strSql.append("	WHERE OrderId = " + orderId);

			getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			mResultCode = mStatement.executeUpdate(strSql.toString());
			
		} catch(SQLException e){
			mResultCode = -1;
			mJsonResponse.setMsgString("Lỗi Exception máy chủ: e = " + e.getMessage());
		} finally {
			this.closeStatement();
		}
		
		mJsonResponse.setMsgCode(mResultCode);
		
		PrintWriter out = response.getWriter();
		out.print(mJsonResponse);
		out.close();
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
