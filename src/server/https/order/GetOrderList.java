package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;

public class GetOrderList extends BaseHttpServlet {
	private static final long serialVersionUID = -6468233831770179555L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONArray data = this.getOrderList();
		PrintWriter out = response.getWriter();
		out.println(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected JSONArray getOrderList() {
		JSONArray jsonArray = new JSONArray();
		try{
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("SELECT TOP 50");
			strSql.append("	 ord.OrderId ");
			strSql.append("	,ord.ModUser ");
			strSql.append("	,ord.ModDt ");
			strSql.append("	,CONVERT(VARCHAR(10), ord.ModDt, 105)  AS ModDate");
			strSql.append("	,CONVERT(VARCHAR(8), ord.ModDt, 108) AS ModTime");
			strSql.append("	,ord.TypeTrans ");
			strSql.append("	,ord.TotalAmount ");
			strSql.append("	,ord.TotalWeight ");
			strSql.append("	,ord.Status ");
			strSql.append("	,(SELECT TOP 1 detail.ProductDesc FROM WS_ORDER_DT detail WHERE detail.OrderId = ord.OrderId) AS ProductDesc");
			strSql.append(" FROM WS_ORDER ord");
			strSql.append(" ORDER BY ord.ModDt DESC ");
			
			getConnectionDb();
			mPreparedStatement = mConnectionDb.prepareStatement(strSql.toString());
			mResultSet = mPreparedStatement.executeQuery();
			
			while(mResultSet.next()){
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("OrderId", mResultSet.getLong("OrderId"));
				jsonObject.put("ModUser", mResultSet.getString("ModUser"));
				jsonObject.put("TypeTrans", mResultSet.getString("TypeTrans"));
				jsonObject.put("TotalAmount", mResultSet.getBigDecimal("TotalAmount"));
				jsonObject.put("TotalWeight", mResultSet.getInt("TotalWeight"));
				jsonObject.put("Status", mResultSet.getString("Status"));
				jsonObject.put("ModDt", mResultSet.getDate("ModDt").toString());
				jsonObject.put("ModDate", mResultSet.getString("ModDate"));
				jsonObject.put("ModTime", mResultSet.getString("ModTime"));
				jsonObject.put("ProductDesc", mResultSet.getString("ProductDesc"));
				jsonArray.add(jsonObject);
			}
			
		} catch(SQLException e){
			jsonArray = null;
		} finally {
			this.closeStatement();
		}
		
		return jsonArray;
	}

}
