package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.https.common.BaseHttpServlet;
import server.utilities.Constants;

public class SendPrinter extends BaseHttpServlet {
	private static final long serialVersionUID = 9040361415766997632L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long orderId = Long.valueOf(request.getParameter("OrderId"));
		int mResultCode = -1;
		
		try{
			StringBuilder strSql = new StringBuilder();
			strSql.append("	UPDATE WS_ORDER SET ");
			strSql.append("		IsPrinter = 1 ");
			strSql.append("		, Status = '" + Constants.STATUS_PRINTED + "'");
			strSql.append("	    , SendPrintDate = GETDATE() ");
			strSql.append("	WHERE OrderId = " + orderId);
			
			getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			mResultCode = mStatement.executeUpdate(strSql.toString());
		} catch(SQLException e){
			mResultCode = -1;
			mJsonResponse.setMsgString("Lỗi Exception từ máy chủ: e = " + e.getMessage());
		} finally {
			closeStatement();
		}
		
		mJsonResponse.setMsgCode(mResultCode);
		
		PrintWriter out = response.getWriter();
		out.print(mJsonResponse);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}	
}