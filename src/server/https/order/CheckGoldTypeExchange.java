package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;


@WebServlet(description = "Check gold type input when exchange transation", urlPatterns = {"/CheckGoldTypeExchange"})
public class CheckGoldTypeExchange extends BaseHttpServlet {
	private static final long serialVersionUID = -6468233831770179555L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String goldType = request.getParameter("GoldType").toString();
		long orderId = Long.valueOf(request.getParameter("OrderId"));
		
		JSONObject data = this.checkSamePrice(goldType, orderId);
	
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@SuppressWarnings("unchecked")
	protected JSONObject checkSamePrice(String goldType, long orderId) {
		JSONObject data = new JSONObject();
		data.put("msgCode", 1);
		data.put("msgString", "Loại vàng chưa được nhập");
		
		try{
			
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("SELECT GoldType ");
			strSql.append("FROM WS_ORDER_DT ");
			strSql.append("WHERE OrderId = " + orderId);
			

			this.getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			while(mResultSet.next()){
				String GoldTypeTmp = mResultSet.getString("GoldType");
				
				if (GoldTypeTmp.length() > 0 && goldType.length() > 0){
					if (false == GoldTypeTmp.equals(goldType)){
						if ((GoldTypeTmp.equals("V600") && goldType.equals("V610")) || (GoldTypeTmp.equals("V610")  && goldType.equals("V600"))){
							data.put("msgCode", 1);
							data.put("msgString", "Đã cùng loại vàng");
						} else {
							data.put("msgCode", -1);
							data.put("msgString", "Khi Mua - Bán đổi vàng, hàng nhập vào phải cùng loại");
							break;
						}
					}
				}
			} 
		} catch(SQLException e){
			data.put("msgCode", -1);
			data.put("msgString", "Lỗi từ máy chủ: e = " +  e.getLocalizedMessage());
		} finally {
			this.closeStatement();
		}
		
		return data;
	}
}
