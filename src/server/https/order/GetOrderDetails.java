package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;


public class GetOrderDetails extends BaseHttpServlet {
	private static final long serialVersionUID = -9141191057035433741L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long orderId = Long.parseLong(request.getParameter("OrderId").toString());
		long orderDetailId = Long.parseLong(request.getParameter("OrderDetailId").toString());
		
		JSONArray data = this.getOrderDetails(orderId, orderDetailId);
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected JSONArray getOrderDetails(long orderId, long orderDetailId) {
		JSONArray jsonArray = new JSONArray();
		
		try{
			StringBuilder strSql = new StringBuilder();
			
			strSql.append("SELECT	 ");
		      strSql.append("	 orderDT.[OrderDetailId] ");
		      strSql.append("	,orderDT.[OrderId] ");
		      strSql.append("	,orderDT.[ModUser] ");
		      strSql.append("	,orderDT.[ModDt] ");
		      strSql.append("	,orderDT.[Shared] ");
		      strSql.append("	,orderDT.[TrnID] ");
		      strSql.append("	,orderDT.[ProductCode] ");
		      strSql.append("	,orderDT.[ProductDesc] ");
		      strSql.append("	,orderDT.[DiamondWeight] ");
		      strSql.append("	,orderDT.[TotalWeightDT] ");
		      strSql.append("	,orderDT.[PriceDT] ");
		      strSql.append("	,orderDT.[PayBonus] ");
		      strSql.append("	,orderDT.[PayBonusAmount] ");
		      strSql.append("	,orderDT.[Salary] ");
		      strSql.append("	,orderDT.[AmountDT] ");
		      strSql.append("	,orderDT.[GroupID] ");
		      strSql.append("	,grp.[GroupName] ");
		      strSql.append("	,orderDT.[GoldType] ");
		      strSql.append("	,typ.[GoldDesc] ");
		      strSql.append("	,orderDT.[Depot] ");
		      strSql.append("	,orderDT.[TypeTrans] ");
			  strSql.append(" FROM WS_ORDER_DT orderDT ");
			  strSql.append(" 	LEFT JOIN WS_TYPE typ ");
			  strSql.append(" 		ON orderDT.GoldType = typ.GoldCode ");
			  strSql.append(" 	LEFT JOIN WS_GROUP grp ");
			  strSql.append(" 		ON orderDT.GroupID = grp.GroupID ");
			  strSql.append(" WHERE 1 = 1");
			  if (orderId > 0){
				  strSql.append(" 	AND orderDT.OrderId = " + orderId);
			  }
			  if (orderDetailId > 0){
				  strSql.append(" 	AND orderDT.OrderDetailId = " + orderDetailId);
			  }
			
			getConnectionDb();
			mPreparedStatement = this.mConnectionDb.prepareStatement(strSql.toString());
			mResultSet = mPreparedStatement.executeQuery();
			
			while(mResultSet.next()){
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("OrderDetailId", mResultSet.getLong("OrderDetailId"));
				jsonObject.put("OrderId", mResultSet.getLong("OrderId"));
				jsonObject.put("ModUser", mResultSet.getString("ModUser"));
				jsonObject.put("ModDt", mResultSet.getDate("ModDt").toString());
				
				jsonObject.put("Shared", mResultSet.getInt("Shared"));
				jsonObject.put("TrnID", mResultSet.getString("TrnID"));
				jsonObject.put("ProductCode", mResultSet.getString("ProductCode"));
				jsonObject.put("ProductDesc", mResultSet.getString("ProductDesc"));
				jsonObject.put("DiamondWeight", mResultSet.getDouble("DiamondWeight"));
				jsonObject.put("TotalWeightDT", mResultSet.getInt("TotalWeightDT"));
				jsonObject.put("PriceDT", mResultSet.getDouble("PriceDT"));
				jsonObject.put("PayBonus", mResultSet.getDouble("PayBonus"));
				jsonObject.put("PayBonusAmount", mResultSet.getDouble("PayBonusAmount"));
				jsonObject.put("Salary", mResultSet.getDouble("Salary"));
				jsonObject.put("AmountDT", mResultSet.getDouble("AmountDT"));
				
				jsonObject.put("GroupID", mResultSet.getString("GroupID"));
				jsonObject.put("GroupName", mResultSet.getString("GroupName"));
				jsonObject.put("GoldType", mResultSet.getString("GoldType"));
				jsonObject.put("GoldDesc", mResultSet.getString("GoldDesc"));
				jsonObject.put("Depot", mResultSet.getString("Depot"));
				jsonObject.put("TypeTrans", mResultSet.getString("TypeTrans"));
				
				jsonArray.add(jsonObject);
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			this.closeStatement();
		}
		
		return jsonArray;
	}
}
