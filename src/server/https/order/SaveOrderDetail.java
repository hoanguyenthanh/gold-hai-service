package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import server.https.common.BaseHttpServlet;
import server.models.ResultSaveOrder;

public class SaveOrderDetail extends BaseHttpServlet {
	private static final long serialVersionUID = -733931884980761267L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultSaveOrder result = this.insertOrderDetail(request);
		
		String data = new Gson().toJson(result);
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		doGet(request, response);
	}
	
	private ResultSaveOrder insertOrderDetail(HttpServletRequest request){
		ResultSaveOrder result = new ResultSaveOrder();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		
		try{
			String Method = request.getParameter("Method").toString();
			String TypeTrans = request.getParameter("TypeTrans").toString(); //Buy|Sell|Exchange for each Invoice
			String ActionTrans = request.getParameter("ActionTrans").toString(); //Buy|Sell for each Product
			
			
			long orderId = Long.valueOf(request.getParameter("OrderId"));
			long OrderDetailId = Long.valueOf(request.getParameter("OrderDetailId"));
	
			String ModUser = request.getParameter("ModUser").toString();
			String TrnID = request.getParameter("TrnID").toString();
			String ProductCode = request.getParameter("ProductCode").toString();
			String ProductDesc = request.getParameter("ProductDesc").toString();
			
			double DiamondWeight = Double.valueOf(request.getParameter("DiamondWeight"));
			int TotalWeightDT = Integer.valueOf(request.getParameter("TotalWeightDT"));
			int TotalWeightDT_L = Integer.valueOf(request.getParameter("TotalWeightDT_L"));
			int TotalWeightDT_C = Integer.valueOf(request.getParameter("TotalWeightDT_C"));
			int TotalWeightDT_P = Integer.valueOf(request.getParameter("TotalWeightDT_P"));
			int TotalWeightDT_Li = Integer.valueOf(request.getParameter("TotalWeightDT_Li"));
			String TotalWeightDTDesc = request.getParameter("TotalWeightDTDesc");
			
			double PriceDT = Double.valueOf(request.getParameter("PriceDT"));
			double PayBonus = Double.valueOf(request.getParameter("PayBonus"));
			double PayBonusAmount = Double.valueOf(request.getParameter("PayBonusAmount"));
			double Salary = Double.valueOf(request.getParameter("Salary"));
			double AmountDT = Double.valueOf(request.getParameter("AmountDT"));
			
			String Depot = request.getParameter("Depot").toString();
			String GroupID = request.getParameter("GroupID").toString();
			String GoldType = request.getParameter("GoldType").toString();
		
			//Get connection
			getConnectionDb();
			
			String SQL = "{call WS_ORDER_Save(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			cstmt = mConnectionDb.prepareCall(SQL);
			
			cstmt.setString("p_Method", Method);
			cstmt.setLong("p_OrderId", orderId);
			
			cstmt.setLong("p_OrderDetailId", OrderDetailId );
			cstmt.setString("p_TypeTrans", TypeTrans);
			cstmt.setString("p_ActionTrans", ActionTrans);
			cstmt.setString("p_ModUser", ModUser);
			cstmt.setString("p_TrnID", TrnID);
			cstmt.setString("p_ProductCode", ProductCode);
			cstmt.setString("p_ProductDesc", ProductDesc);
			
			cstmt.setDouble("p_DiamondWeight", DiamondWeight);
			cstmt.setInt("p_TotalWeightDT", TotalWeightDT);
			cstmt.setInt("p_TotalWeightDT_L", TotalWeightDT_L);
			cstmt.setInt("p_TotalWeightDT_C", TotalWeightDT_C);
			cstmt.setInt("p_TotalWeightDT_P", TotalWeightDT_P);
			cstmt.setInt("p_TotalWeightDT_Li", TotalWeightDT_Li);
			
			cstmt.setString("p_TotalWeightDTDesc", TotalWeightDTDesc);
			
			cstmt.setDouble("p_PriceDT", PriceDT);
			cstmt.setDouble("p_PayBonus", PayBonus);
			cstmt.setDouble("p_PayBonusAmount", PayBonusAmount);
			cstmt.setDouble("p_Salary", Salary);
			cstmt.setDouble("p_AmountDT", AmountDT);
			
			
			cstmt.setString("p_Depot", Depot);
			cstmt.setString("p_GroupID", GroupID);
			cstmt.setString("p_GoldType", GoldType); // GoldTypeExchange			
			
			boolean results = cstmt.execute();
			int rowsAffected = 0;
			 
	        // Protects against lack of SET NOCOUNT in stored prodedure
	        while (results || rowsAffected != -1) {
	            if (results) {
	            	rs = cstmt.getResultSet();
	                break;
	            } else {
	                rowsAffected = cstmt.getUpdateCount();
	            }
	            results = cstmt.getMoreResults();
	        }
	        while (rs.next()) {
	        	result.OrderId = rs.getLong("OrderId");
				result.OrderDetailId = rs.getLong("OrderDetailId");
				result.Result = rs.getInt("Result");
				result.ErrorDesc = rs.getString("ErrorDesc");
				break;
	        }
		} catch(SQLException e){
			result.ErrorDesc = "Lỗi từ máy chủ: e = " + e.getLocalizedMessage();
			result.Result = -1;
			result.OrderId = 0;
			result.OrderDetailId = 0;
		} finally {
			try{
				if (rs != null){
					rs.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
			
			try{
				if (cstmt != null){
					cstmt.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
			
			try{
				if (mConnectionDb != null){
					mConnectionDb.close();
				}
			} catch(SQLException e){
				e.printStackTrace();
			}
			
			closeStatement();
		}
		
		return result;
	}
	
	protected long insertOrderMaster(Connection connect, String ModUser, String TypeTrans) {
		long orderId = -1;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		try{
			StringBuilder strSql = new StringBuilder();
			strSql.append(" INSERT INTO WS_ORDER(ModUser, ModDt, Status, TypeTrans)");
			strSql.append(" OUTPUT inserted.OrderId ");
			strSql.append(" VALUES ('" + ModUser + "', GETDATE(), 'new', '" + TypeTrans + "')");
			
			statement = connect.prepareStatement(strSql.toString());
			resultSet = statement.executeQuery();
			
			if (resultSet.next()){
				orderId = mResultSet.getLong("OrderId");
			}
			
		} catch(SQLException e){
			orderId = -1;
		} finally {
			try{
				if (statement != null){
					statement.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
			
			try{
				if (resultSet != null){
					resultSet.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		
		return orderId;
	}
	
	protected boolean isDiffGoldTypeInsert(Connection connect, long orderId, String goldType) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int resultCode = 1;
		
		try{
			preparedStatement = connect.prepareStatement("SELECT GoldTypeExchange FROM WS_ORDER WHERE OrderId = " + orderId);
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()){
				String goldTypeExchange = resultSet.getString("GoldTypeExchange");
				
				if (goldTypeExchange == null || goldTypeExchange.length() == 0){
					resultCode = -1;//xem như không khác loại vàng
				} else {
					if (goldType == null) goldType = "";
					if (goldTypeExchange.length() > 0 && goldType.length() > 0 && !goldTypeExchange.equals(goldType)){
						if ((goldTypeExchange.equals("V600") && goldType.equals("V610")) 
								|| (goldTypeExchange.equals("V610")  && goldType.equals("V600"))){
							resultCode = -1;// Same gold type
						}
					}
				}
			}
		} catch(SQLException e){
			resultCode = 1;
		} finally {
			try {
				if (preparedStatement != null){
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try{
				if (resultSet != null){
					resultSet.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		
		return resultCode > 0;
	}
}
