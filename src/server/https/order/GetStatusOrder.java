package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.https.common.BaseHttpServlet;
import server.https.common.JsonResponse;


@WebServlet(description = "Lấy trạng thái order", urlPatterns = { "/GetStatusOrder" })
public class GetStatusOrder extends BaseHttpServlet {
	private static final long serialVersionUID = 7741744610360656518L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long orderId = Long.valueOf(request.getParameter("OrderId"));
		
		mJsonResponse = new JsonResponse();
		try{
			getConnectionDb();
			mPreparedStatement = mConnectionDb.prepareStatement("SELECT Status FROM WS_ORDER WHERE OrderId = " + orderId);
			mResultSet = mPreparedStatement.executeQuery();
			if (mResultSet.next()){
				String status = mResultSet.getString("Status");
				mJsonResponse.setStatus(status);
			}
			
		} catch (SQLException e) {
			mJsonResponse.setStatus("");
		} finally {
			closeStatement();
		}
		
		PrintWriter out = response.getWriter();
		out.print(mJsonResponse);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
