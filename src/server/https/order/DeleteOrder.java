package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.https.common.BaseHttpServlet;
import server.https.common.JsonResponse;

public class DeleteOrder extends BaseHttpServlet {
	private static final long serialVersionUID = 9040361415766997632L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long orderId = Long.parseLong(request.getParameter("OrderId").toString());
		
		mJsonResponse = this.deleteOrder(orderId);
		
		PrintWriter out = response.getWriter();
		out.print(mJsonResponse);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	
	protected JsonResponse deleteOrder(long orderId) {
		
		JsonResponse result = new JsonResponse();
		CallableStatement callableStatement = null;
		
		try{
			this.getConnectionDb();
			
			callableStatement = mConnectionDb.prepareCall("{ call WS_ORDER_Delete(?, ?, ?)}");
			callableStatement.setLong("p_OrderId", orderId);
			
			callableStatement.registerOutParameter("p_Result", java.sql.Types.INTEGER);
			callableStatement.registerOutParameter("p_ErrorDesc", java.sql.Types.VARCHAR);
			
			callableStatement.execute();
			
			int rsl = callableStatement.getInt("p_Result");
			String error = callableStatement.getString("p_ErrorDesc");
			
			result.setMsgCode(rsl);
			result.setMsgString(error);
			
		} catch(SQLException e){
			result.setMsgCode(-1);
			result.setMsgString("Lỗi từ máy chủ: e = " + e.getLocalizedMessage());
		} finally {
			try {
				if (callableStatement != null){
					callableStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			this.closeStatement();
		}
		
		return result;
	}
}