package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.https.common.BaseHttpServlet;
import server.https.common.JsonResponse;

public class UpdateInfoPrint extends BaseHttpServlet {
	private static final long serialVersionUID = 9040361415766997632L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int resultCode = -1;
		mJsonResponse = new JsonResponse();
		
		Statement statement = null;
		
		try{
			long orderId = Long.parseLong(request.getParameter("OrderId").toString());
			
			int SellTotalWeight = Integer.valueOf(request.getParameter("SellTotalWeight"));
			int SellTotalWeight_L = Integer.valueOf(request.getParameter("SellTotalWeight_L"));
			int SellTotalWeight_C = Integer.valueOf(request.getParameter("SellTotalWeight_C"));
			int SellTotalWeight_P = Integer.valueOf(request.getParameter("SellTotalWeight_P"));
			int SellTotalWeight_Li = Integer.valueOf(request.getParameter("SellTotalWeight_Li"));
			String SellTotalWeightDesc = request.getParameter("SellTotalWeightDesc");
			
			int BuyTotalWeight = Integer.valueOf(request.getParameter("BuyTotalWeight"));
			int BuyTotalWeight_L = Integer.valueOf(request.getParameter("BuyTotalWeight_L"));
			int BuyTotalWeight_C = Integer.valueOf(request.getParameter("BuyTotalWeight_C"));
			int BuyTotalWeight_P = Integer.valueOf(request.getParameter("BuyTotalWeight_P"));
			int BuyTotalWeight_Li = Integer.valueOf(request.getParameter("BuyTotalWeight_Li"));
			String BuyTotalWeightDesc = request.getParameter("BuyTotalWeightDesc");
			
			int TotalWeight = Integer.valueOf(request.getParameter("TotalWeight"));
			int TotalWeight_L = Integer.valueOf(request.getParameter("TotalWeight_L"));
			int TotalWeight_C = Integer.valueOf(request.getParameter("TotalWeight_C"));
			int TotalWeight_P = Integer.valueOf(request.getParameter("TotalWeight_P"));
			int TotalWeight_Li = Integer.valueOf(request.getParameter("TotalWeight_Li"));
			String TotalWeightDesc = request.getParameter("TotalWeightDesc");
			
			double PriceSell = Double.valueOf(request.getParameter("PriceSell"));
			double PriceBuy = Double.valueOf(request.getParameter("PriceBuy"));
			double Amount = Double.valueOf(request.getParameter("Amount"));
			double TotalSalary = Double.valueOf(request.getParameter("TotalSalary"));
			double TotalPayBonus = Double.valueOf(request.getParameter("TotalPayBonus"));
			double TotalAmount = Double.valueOf(request.getParameter("TotalAmount"));
			
			
			StringBuilder strSql = new StringBuilder();
			strSql.append("	UPDATE WS_ORDER SET ");
			strSql.append("		 SellTotalWeight = " + SellTotalWeight);
			strSql.append("		,SellTotalWeight_L = " + SellTotalWeight_L);
			strSql.append("		,SellTotalWeight_C = " + SellTotalWeight_C);
			strSql.append("		,SellTotalWeight_P = " + SellTotalWeight_P);
			strSql.append("		,SellTotalWeight_Li = " + SellTotalWeight_Li);
			strSql.append("		,SellTotalWeightDesc = '" + SellTotalWeightDesc + "'");
			strSql.append("		,BuyTotalWeight = " + BuyTotalWeight);
			strSql.append("		,BuyTotalWeight_L = " + BuyTotalWeight_L);
			strSql.append("		,BuyTotalWeight_C = " + BuyTotalWeight_C);
			strSql.append("		,BuyTotalWeight_P = " + BuyTotalWeight_P);
			strSql.append("		,BuyTotalWeight_Li = " + BuyTotalWeight_Li);
			strSql.append("		,BuyTotalWeightDesc = '" + BuyTotalWeightDesc + "'");
			strSql.append("		,TotalWeight = " + TotalWeight);
			strSql.append("		,TotalWeight_L = " + TotalWeight_L);
			strSql.append("		,TotalWeight_C = " + TotalWeight_C);
			strSql.append("		,TotalWeight_P = " + TotalWeight_P);
			strSql.append("		,TotalWeight_Li = " + TotalWeight_Li);
			strSql.append("		,TotalWeightDesc = '" + TotalWeightDesc + "'");
			strSql.append("		,PriceSell = " + PriceSell);
			strSql.append("		,PriceBuy = " + PriceBuy);
			strSql.append("		,Amount = " + Amount);
			strSql.append("		,TotalSalary = " + TotalSalary);
			strSql.append("		,TotalPayBonus = " + TotalPayBonus);
			strSql.append("		,TotalAmount = " + TotalAmount);
			strSql.append("	WHERE OrderId = " + orderId);
			
			getConnectionDb();
			statement = mConnectionDb.createStatement();
			resultCode = statement.executeUpdate(strSql.toString());
			
		} catch(SQLException e){
			resultCode = -1;
			mJsonResponse.setMsgString("Lỗi Exception từ máy chủ: e = " + e.getLocalizedMessage());
		} finally {
			try{
				if (statement != null){
					statement.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
			
			try{
				if (mConnectionDb != null){
					mConnectionDb.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
			
			closeStatement();
		}
		
		
		mJsonResponse.setMsgCode(resultCode);
	
		PrintWriter out = response.getWriter();
		out.print(mJsonResponse);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}	
}