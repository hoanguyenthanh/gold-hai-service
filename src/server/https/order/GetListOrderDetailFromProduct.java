package server.https.order;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;

public class GetListOrderDetailFromProduct extends BaseHttpServlet {
	private static final long serialVersionUID = -9141191057035433741L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long orderId = Long.parseLong(request.getParameter("OrderId").toString());
		
		JSONArray data = getProductsOld(orderId);
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@SuppressWarnings("unchecked")
	protected JSONArray getProductsOld(long orderId) {
		JSONArray jsonArray = new JSONArray();
		
		try{
			StringBuilder strSql = new StringBuilder();
			strSql.append("SELECT	 ");
			  strSql.append("	prodDt.[ProductCode] ");
			  strSql.append("	,prod.[TrnID] ");
		      strSql.append("	,prod.[InOut] ");
		      strSql.append("	,prod.[TrnDate] ");
		      strSql.append("	,prod.[TrnTime] ");
		      strSql.append("	,prod.[SectionID] ");
		      strSql.append("	,prod.[GoldCode] ");
		      strSql.append("	,prod.[PriceCcy] ");
		      strSql.append("	,prod.[PriceUnit] ");
		      strSql.append("	,prod.[ProductDesc] ");
		      strSql.append("	,prod.[GroupID] ");
		      strSql.append("	,prod.[Quantity] ");
		      strSql.append("	,prod.[TotalWeight] ");
		      strSql.append("	,prod.[TotalWeightRounding] ");
		      strSql.append("	,prod.[DiamondWeight] ");
		      strSql.append("	,prod.[StampWeight] ");
		      strSql.append("	,prod.[TaskPrice] ");
		      strSql.append("	,prod.[POTaskPrice1] ");
		      strSql.append("	,prod.[POTaskPrice2] ");
		      strSql.append("	,prod.[RingSize] ");
		      strSql.append("	,prod.[InPrice] ");
		      strSql.append("	,prod.[BuyPrice] ");
		      strSql.append("	,prod.[Reson] ");
		      strSql.append("	,prod.[UserID] ");
		      strSql.append("	,prod.[AuthUserID] ");
		      strSql.append("	,prod.[Status] ");
		      strSql.append("	,prod.[Printed] ");
		      strSql.append("	,prod.[CongVon] ");
		      strSql.append("	,prod.[GiaVangVon] ");
		      strSql.append("	,prod.[KyHieu] ");
		      strSql.append("	,prod.[KiemKe] ");
		      strSql.append("	,prod.[NCC] ");
		      
		      strSql.append("	,orderDT.[OrderDetailId] ");
		      strSql.append("	,orderDT.[OrderId] ");
		      strSql.append("	,orderDT.[ModUser] ");
		      strSql.append("	,orderDT.[ModDt] ");
		      strSql.append("	,orderDT.[PayBonus] ");
		      strSql.append("	,orderDT.[Salary] ");
		      
			  strSql.append("FROM WS_ORDER_DT orderDT ");
			  strSql.append("	INNER JOIN TRN_PRODUCT_IN_DT prodDt ");  
			  strSql.append("		ON orderDT.ProductCode = prodDt.ProductCode ");
			  strSql.append("		AND orderDT.TrnID = prodDt.TrnID ");
			  strSql.append("	INNER JOIN TRN_PRODUCT_IN prod");
			  strSql.append("		ON orderDT.TrnID = prod.TrnID ");
			  strSql.append("WHERE orderDT.OrderId = " + orderId);
			
			getConnectionDb();
			mPreparedStatement = mConnectionDb.prepareStatement(strSql.toString());
			mResultSet = mPreparedStatement.executeQuery();
			
			while(mResultSet.next()){
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("ProductCode", mResultSet.getString("ProductCode"));
				jsonObject.put("TrnID", mResultSet.getString("TrnID"));
				jsonObject.put("InOut", mResultSet.getString("InOut"));
				jsonObject.put("TrnDate", mResultSet.getDate("TrnDate").toString());
				jsonObject.put("TrnTime", mResultSet.getString("TrnTime").toString());
				jsonObject.put("SectionID", mResultSet.getString("SectionID"));
				jsonObject.put("GoldCode", mResultSet.getString("GoldCode"));
				jsonObject.put("PriceCcy", mResultSet.getString("PriceCcy"));
				jsonObject.put("PriceUnit", mResultSet.getString("PriceUnit"));
				jsonObject.put("ProductDesc", mResultSet.getString("ProductDesc"));
				jsonObject.put("GroupID", mResultSet.getString("GroupID"));
				jsonObject.put("Quantity", mResultSet.getBigDecimal("Quantity"));
				jsonObject.put("TotalWeight", mResultSet.getBigDecimal("TotalWeight"));
				jsonObject.put("TotalWeightRounding", mResultSet.getInt("TotalWeightRounding"));
				jsonObject.put("DiamondWeight", mResultSet.getBigDecimal("DiamondWeight"));
				jsonObject.put("StampWeight", mResultSet.getBigDecimal("StampWeight"));
				jsonObject.put("TaskPrice", mResultSet.getBigDecimal("TaskPrice"));
				jsonObject.put("POTaskPrice1", mResultSet.getBigDecimal("POTaskPrice1"));
				jsonObject.put("POTaskPrice2", mResultSet.getBigDecimal("POTaskPrice2"));
				jsonObject.put("RingSize", mResultSet.getBigDecimal("RingSize"));
				jsonObject.put("InPrice", mResultSet.getBigDecimal("InPrice"));
				jsonObject.put("BuyPrice", mResultSet.getBigDecimal("BuyPrice"));					
				jsonObject.put("Reson", mResultSet.getString("Reson"));
				jsonObject.put("UserID", mResultSet.getString("UserID"));
				jsonObject.put("AuthUserID", mResultSet.getString("AuthUserID"));
				jsonObject.put("Status", mResultSet.getString("Status"));
				jsonObject.put("Printed", mResultSet.getString("Printed"));
				jsonObject.put("CongVon", mResultSet.getBigDecimal("CongVon"));
				jsonObject.put("GiaVangVon", mResultSet.getBigDecimal("GiaVangVon"));
				jsonObject.put("KyHieu", mResultSet.getString("KyHieu"));
				jsonObject.put("KiemKe", mResultSet.getString("KiemKe"));
				jsonObject.put("NCC", mResultSet.getString("NCC"));
				
				jsonObject.put("OrderDetailId", mResultSet.getLong("OrderDetailId"));
				jsonObject.put("OrderId", mResultSet.getLong("OrderId"));
				jsonObject.put("ModUser", mResultSet.getString("ModUser"));
				jsonObject.put("ModDt", mResultSet.getDate("ModDt").toString());
				jsonObject.put("PayBonus", mResultSet.getBigDecimal("PayBonus"));
				jsonObject.put("Salary", mResultSet.getBigDecimal("Salary"));
				
				jsonArray.add(jsonObject);
			}
		} catch(SQLException e){
			jsonArray = null;
		} finally {
			this.closeStatement();
		}
		
		return jsonArray;
	}

}
