package server.https.login;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;

//@WebServlet(description = "API login", urlPatterns = { "/Login" })

public class Login extends BaseHttpServlet {
	private static final long serialVersionUID = 4030001322644273491L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId = request.getParameter("UserName").toString();
		String pwd = request.getParameter("Password").toString();
		
		JSONObject userInfo = this.getUserInfo(userId, pwd);
		
		PrintWriter out = response.getWriter();
		out.print(userInfo);
		out.close();
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	
	@SuppressWarnings("unchecked")
	protected JSONObject getUserInfo(String userId, String password) {
		JSONObject jsonData = new JSONObject();
		
		try
		{
			getConnectionDb();
			mPreparedStatement = mConnectionDb.prepareStatement("select * from WS_USERS where UserName = ? and Password = ?");
			mPreparedStatement.setString(1, userId);
			mPreparedStatement.setString(2, password);
			
			mResultSet = mPreparedStatement.executeQuery();
			
			if(mResultSet.next()){
				jsonData.put("UserID", mResultSet.getLong("UserID"));
				jsonData.put("UserName", mResultSet.getString("UserName"));
				jsonData.put("Password", mResultSet.getInt("Password"));
				jsonData.put("FullName", mResultSet.getString("FullName"));
				jsonData.put("Email", mResultSet.getString("Email"));
				jsonData.put("Mobile", mResultSet.getString("Mobile"));
				jsonData.put("LastVisited", mResultSet.getString("LastVisited"));
				jsonData.put("IsAdmin", mResultSet.getInt("IsAdmin"));
				jsonData.put("Active", mResultSet.getInt("Active"));
			}
			
		} catch  (SQLException e){
			jsonData = null;
		} finally {
			this.closeStatement();
		}
		
		return jsonData;
	}
}