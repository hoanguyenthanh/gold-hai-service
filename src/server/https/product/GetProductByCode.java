package server.https.product;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;


public class GetProductByCode extends BaseHttpServlet {
	
	private static final long serialVersionUID = 3494969099684817442L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String productCode = request.getParameter("ProductCode");
		
		JSONObject data = this.getProductByCode(productCode);
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected JSONObject getProductByCode(String productCode) {
		
		JSONObject jsonObject = new JSONObject();
		if (productCode == null) productCode = "";
		try{	
			StringBuilder strSql = new StringBuilder();
			strSql.append(" SELECT");
			strSql.append("	 prod.ProductCode ");
			strSql.append("	,prod.ProductDesc ");
		    strSql.append("	,prod.GoldCode ");
		    strSql.append("	,prod.GroupID ");
		    strSql.append("	,grp.GroupName ");
		    strSql.append("	,prod.GoldWeight ");
		    strSql.append("	,prod.DiamondWeight ");
		    strSql.append("	,prod.TotalWeight ");
			strSql.append(" FROM WS_PRODUCT_IN prod ");
			strSql.append("		LEFT JOIN WS_GROUP grp");
			strSql.append("			ON grp.GroupID = prod.GroupID ");
			strSql.append(" WHERE prod.ProductCode = '" + productCode + "'");
			
			getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			while(mResultSet.next()){
				jsonObject.put("ProductCode", mResultSet.getString("ProductCode"));
				jsonObject.put("ProductDesc", mResultSet.getString("ProductDesc"));
				
				jsonObject.put("GoldCode", mResultSet.getString("GoldCode"));
				jsonObject.put("GroupID", mResultSet.getString("GroupID"));
				jsonObject.put("GroupName", mResultSet.getString("GroupName"));
		
				jsonObject.put("GoldWeight", mResultSet.getBigDecimal("GoldWeight"));
				jsonObject.put("DiamondWeight", mResultSet.getBigDecimal("DiamondWeight"));
				jsonObject.put("TotalWeight", mResultSet.getBigDecimal("TotalWeight"));
				
				break;
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			this.closeStatement();
		}
		
		return jsonObject;
	}

}
