package server.https.product;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import server.https.common.BaseHttpServlet;


@WebServlet(description = "Search product", urlPatterns = {"/SearchProduct"})
public class SearchProduct extends BaseHttpServlet {
	private static final long serialVersionUID = 8065563688341193897L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String placeSearch = request.getParameter("PlaceSearch").toString();
		String textSearch = request.getParameter("TextSearch").toString();
		String codeSearch = request.getParameter("CodeSearch").toString();
		
		JSONArray data = "old".equals(placeSearch) ?  getProductOld(textSearch, codeSearch) : getProductNew(codeSearch);
		
		PrintWriter out = response.getWriter();
		out.print(data);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected JSONArray getProductOld(String textSearch, String codeSearch) {
		JSONArray jsonArray = new JSONArray();

		try {
			textSearch = textSearch.trim();
			codeSearch = codeSearch.trim();
		
			StringBuilder strSql = new StringBuilder();
			
			
			strSql.append(" SELECT TOP 1000 detail.ProductCode, prod.ProductDesc, prod.TotalWeight, prod.GoldCode ");
			strSql.append(" FROM dbo.TRN_PRODUCT_IN prod ");
			strSql.append("		INNER JOIN TRN_PRODUCT_IN_DT detail on prod.TrnID = detail.TrnID ");
			strSql.append(" WHERE 1=1 ");
			
			if (textSearch.length() > 0){
				strSql.append("	AND SUBSTRING(detail.ProductCode, 1, 2) like '" + textSearch + "%'");
			}
			
			if (codeSearch.length() > 0){
				strSql.append("	AND SUBSTRING(detail.ProductCode, 3, 10) like '%" + codeSearch + "%'");
			}
			
			strSql.append(" ORDER BY prod.TrnDate desc");

			getConnectDbOLD();
			
			mStatement = mConnectionDb.createStatement();
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			while (mResultSet.next()) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("ProductCode", mResultSet.getString("ProductCode"));
				jsonObject.put("ProductDesc", mResultSet.getString("ProductDesc"));
				jsonObject.put("TotalWeight", mResultSet.getDouble("TotalWeight"));
				jsonObject.put("GoldCode", mResultSet.getString("GoldCode"));
				jsonArray.add(jsonObject);
			}
		} catch (SQLException e) {
			jsonArray = null;
			e.printStackTrace();
		} finally {
			this.closeStatement();
		}

		return jsonArray;
	}
	
	
	@SuppressWarnings("unchecked")
	protected JSONArray getProductNew(String productCode) {
		
		JSONArray jsonArray = new JSONArray();
		try {
			productCode = productCode.trim();
			
			StringBuilder strSql = new StringBuilder();
			strSql.append(" SELECT TOP 500 prod.ProductCode, prod.ProductDesc, prod.TotalWeight, prod.GoldCode  ");
			strSql.append("	 	FROM dbo.WS_PRODUCT_IN prod ");
			strSql.append(" WHERE 1 = 1 ");
			
			if (productCode.length() > 0){
				strSql.append("		AND prod.ProductCode like '%" + productCode + "%'");
			}
			strSql.append(" ORDER BY prod.ModDt desc ");

			getConnectionDb();
			mStatement = mConnectionDb.createStatement();
			mResultSet = mStatement.executeQuery(strSql.toString());
			
			while (mResultSet.next()) {
				JSONObject object = new JSONObject();
				object.put("ProductCode", mResultSet.getString("ProductCode"));
				object.put("ProductDesc", mResultSet.getString("ProductDesc"));
				object.put("TotalWeight", mResultSet.getDouble("TotalWeight"));
				object.put("GoldCode", mResultSet.getString("GoldCode"));
				jsonArray.add(object);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			jsonArray = null;
		} finally {
			closeStatement();
		}

		return jsonArray;
	}
}
