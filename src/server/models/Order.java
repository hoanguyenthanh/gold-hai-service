package server.models;

import net.sourceforge.jtds.jdbc.DateTime;

public class Order {
	public long OrderId;
	public String ModUser;
	public DateTime  ModDt;
	public String Status;
	public String TypeTrans;
	public int IsPrinter;
	public DateTime SendPrintDate;
	public int SellTotalWeight;
	public String SellTotalWeightDesc;
	public int SellTotalWeight_L;
	public int SellTotalWeight_C;
	public int SellTotalWeight_P;
	public int SellTotalWeight_Li;
	public int BuyTotalWeight;
	public String BuyTotalWeightDesc;
	public int BuyTotalWeight_L;
	public int BuyTotalWeight_C;
	public int BuyTotalWeight_P;
	public int BuyTotalWeight_Li;
	public int TotalWeight;
	public String TotalWeightDesc;
	public int TotalWeight_L;
	public int TotalWeight_C;
	public int TotalWeight_P;
	public int TotalWeight_Li;
	public long PriceSell;
	public long PriceBuy;
	public long Amount;
	public long TotalSalary;
	public long TotalPayBonus;
	public long TotalAmount;
}
