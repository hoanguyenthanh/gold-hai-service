package server.utilities;

public class MyUtil {
	
	public static void logExecuteLogic(String className, String error){
		String result;
		if (error == null){
			result = "-";
		} else {
			result = String.format("%s execute logic error: %s", className, error);
		}
		
		System.out.println(result);
	}
}
