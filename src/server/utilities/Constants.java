package server.utilities;

public class Constants {
	
	public static final String STATUS_NEW = "new";
    public static final String STATUS_PRINTED = "printed";
    public static final String STATUS_CANCELED = "canceled";
    public static final String STATUS_PAID = "paid";
    public static final String STATUS_NOT_FOUND = "not_found";
	

	public static int ACTION_INSERT = 1;
	public static int ACTION_UPDATE = 2;
	public static int ACTION_DELETE = 3;
	
	public static final int CODE_RESULT_SUCCESS = 1;
    public static final int CODE_RESULT_ERROR = -1;
    public static final int CODE_RESULT_COMFIRM = -2;
    public static final int CODE_RESULT_NOT_FOUND_DATA = -3;
    public static final int CODE_RESULT_NOTHING = 0;

    public static final String RESULT_CODE = "result_code";
    public static final String RESULT_MSG = "result_msg";
    public static final String RESULT_DATA = "result_data";
    public static final String RESULT_GENERATED_KEY = "result_generated_key";

    public static final int OBJECT_RESULT_JSON_OBJECT = 1;
    public static final int OBJECT_RESULT_JSON_ARRAY = 2;
	
	//Display item on menu
	public static int ITEM_DISPLAY_MENU_YES = 1;
	
	//Not display item on menu
	public static int ITEM_DISPLAY_MENU_NO = 0;
	
	public static final String TYPE_SELL = "sell";
    public static final String TYPE_BUY = "buy";
    public static final String TYPE_SELL_AND_BUY = "sell_and_buy";
}