package server.utilities;

public class ColumnName {
	//Common
	public static String NOTE = "note";
	public static String STATUS = "status";
	public static String USER__CREATE = "user_create";
	public static String USER__MDF = "user_mdf";
	public static String DATE__CREATE = "date_create";
	public static String DATE__MDF = "date_mdf";
	public static String BRANCH_ID = "branch_id";

	//Staff
	public static String STAFF__STAFF_ID = "staff_id";
	public static String STAFF__STAFF_NAME = "staff_name";
	public static String STAFF__STAFF_CODE = "staff_cd";
	public static String STAFF__USER_ID = "user_id";
	public static String STAFF__PASSWORD = "password";
	
	//Order
	public static String ORDER__ORDER_ID = "order_id";
	public static String ORDER__TABLE_ID = "table_id";
	public static String ORDER__TIME_IN = "time_in";
	public static String ORDER__TIME_OUT = "time_out";
	public static String ORDER__TOTAL_AMOUNT = "total_amount";
	
	//Order Detail
	public static String ORDERDT__ORDER_DT_ID = "orderdt_id";
	public static String ORDERDT__ORDER_ID = "order_id";
	public static String ORDERDT__ITEM_ID = "itm_id";
	public static String ORDERDT__QTY_OUT = "qty_out";
	public static String ORDERDT__PRICE = "price";
	public static String ORDERDT__AMOUNT = "amount";
	public static String ORDERDT__UNIT_ID = "unit_id";
	public static String ORDERDT__ITEM_NAME = "itm_name";
	public static String ORDERDT__UNIT_NAME = "unit_name";
	
	//Item
	public static String ITEM__ITEM_ID = "itm_id";
	public static String ITEM__ITEM_CODE = "itm_cd";
	public static String ITEM__ITEM_NAME = "itm_name";
	public static String ITEM__PRICE = "price";
	public static String ITEM__UNIT_ID = "unit_id";
	public static String ITEM__QTY_OUT = "qty_out";
	public static String ITEM__QTY_IN = "qty_in";
	public static String ITEM__QTY_INTY = "qty_inty";
	
	public static String ITEM__GROUP_ID = "itm_grp_id";
	public static String ITEM__GROUP_CODE = "itm_grp_cd";
	public static String ITEM__GROUP_NAME = "name";
	
	//Area
	public static String AREA__ID = "area_id";
	public static String AREA__NAME = "area_name";
	
	//Table
	public static String TABLE_ID = "table_id";
	public static String TABLE_NAME = "table_name";
	public static String TABLE_AREA_ID = "area_id";
	
	//UNIT
	public static String UNIT_ID = "unit_id";
}
